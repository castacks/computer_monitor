#!/usr/bin/python
import rospy
import subprocess
import os
from diagnostic_msgs.msg import DiagnosticStatus, DiagnosticArray, KeyValue

def get_hd_info(filesystem):
    hd_use = 0
    hd_used = ''
    hd_avail = ''
    df_output = subprocess.check_output("df -h", shell=True)
    lines = df_output.split('\n')
    for line in lines:
        if filesystem in line:
            hd_use = int(line.split()[4].replace('%', ''))
            hd_used = line.split()[2]
            hd_avail = line.split()[3]
    return hd_use, hd_used, hd_avail

if __name__ == '__main__':
    rospy.init_node('hd_monitor', anonymous=True)

    filesystem = rospy.get_param('filesystem', '/dev/sda2')
    use_warn = rospy.get_param("~use_warn", 90)
    use_error = rospy.get_param("~use_error", 95)
    
    diag_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
    array = DiagnosticArray()
    status = DiagnosticStatus()
    status.name = "HD Usage"
    status.hardware_id = filesystem

    hd_use_kv = KeyValue()
    hd_use_kv.key = "HD Use Percentage"
    hd_use_kv.value = ""
    hd_used_kv = KeyValue()
    hd_used_kv.key = "Used Space"
    hd_used_kv.value = ""
    hd_avail_kv = KeyValue()
    hd_avail_kv.key = "Available Space"
    hd_avail_kv.value = ""

    rate = rospy.Rate(1.)
    
    while not rospy.is_shutdown():
        hd_use, hd_used, hd_avail = get_hd_info(filesystem)
        hd_use_kv.value = str(hd_use)
        hd_used_kv.value = hd_used
        hd_avail_kv.value = hd_avail
        
        status.level = DiagnosticStatus.OK
        status.message = "OK"
        if hd_use >= use_warn:
            status.level = DiagnosticStatus.WARN
            status.message = "Warning: HD usage is >= " + str(use_warn) + "%"
        if hd_use >= use_error:
            status.level = DiagnosticStatus.ERROR
            status.message = "Error: HD usage is >= " + str(use_error) + "%"
        
        status.values = [hd_use_kv, hd_used_kv, hd_avail_kv]
        
        array.header.stamp = rospy.Time.now()
        array.status = [status]
        
        diag_pub.publish(array)
        rate.sleep()
        
