#!/usr/bin/python
import rospy
import subprocess
import os
from diagnostic_msgs.msg import DiagnosticStatus, DiagnosticArray, KeyValue

def get_chrony_info():
    running, offset, ref_id, last_rx  = True, 0., '', ''
    try:
        chrony_output = subprocess.check_output("chronyc sources", shell=True)
    except:
        running = False
        return running, offset, ref_id, last_rx
    
    lines = chrony_output.split('\n')
    for line in lines:
        if '*' in line:
            # example line: #* NEMA  0   4   377    13   -504us[ -399us] +/-  250ms
            cols = line.split()
            cols = cols[:6] + [''.join(cols[6:])] # merge the '-504us[ -399us] +/-  250ms' column

            offset = cols[-1].split('[')[0]
            unit_str = ''
            factor = 0.
            if 'ns' in offset:
                unit_str = 'ns'
                factor = 1e-9
            elif 'us' in offset:
                unit_str = 'us'
                factor = 1e-6
            elif 'ms' in offset:
                unit_str = 'ms'
                factor = 1e-3
            elif 's' in offset:
                unit_str = 's'
                factor = 1.

            offset = factor*float(offset.replace(unit_str, ''))
            
            ref_id = cols[1]
            last_rx = cols[5]
    return running, offset, ref_id, last_rx



if __name__ == '__main__':
    rospy.init_node('chrony_monitor', anonymous=True)

    error_seconds = rospy.get_param("~seconds_error", 0.001)
    
    diag_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
    array = DiagnosticArray()
    status = DiagnosticStatus()
    status.name = "Chrony Status"
    status.hardware_id = "Clock"
    
    offset_kv = KeyValue()
    offset_kv.key = "Offset (seconds)"
    offset_kv.value = ""
    ref_id_kv = KeyValue()
    ref_id_kv.key = "Reference ID"
    ref_id_kv.value = ""
    last_rx_kv = KeyValue()
    last_rx_kv.key = "Last Received"
    last_rx_kv.value = ""

    rate = rospy.Rate(1.)
    
    while not rospy.is_shutdown():
        running, offset, ref_id, last_rx = get_chrony_info()
        offset_kv.value = str(offset)
        ref_id_kv.value = ref_id
        last_rx_kv.value = last_rx
        
        status.level = DiagnosticStatus.OK
        status.message = "OK"
        if not running:
            status.level = DiagnosticStatus.ERROR
            status.message = 'Chrony not running'
        elif abs(offset) >= error_seconds:
            status.level = DiagnosticStatus.ERROR
            status.message = "Error: Time offset is >= " + str(error_seconds)
        
        status.values = [offset_kv, ref_id_kv, last_rx_kv]
        
        array.header.stamp = rospy.Time.now()
        array.status = [status]
        
        diag_pub.publish(array)
        rate.sleep()
        
